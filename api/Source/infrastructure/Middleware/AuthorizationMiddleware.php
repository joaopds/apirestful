<?php
namespace infrastructure\Middleware;

use Tuupola\Middleware\JwtAuthentication;
class AuthorizationMiddleware
{
    static function AuthorizationToken()
    {
        return new JwtAuthentication([
            "secret" => $_SERVER['JWT_KEY']
        ]);
    }
}


