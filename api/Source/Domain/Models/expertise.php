<?php


namespace Source\Domain\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Expertise extends Eloquent
{

    public $timestamps = false;
    protected $table = 'expertise_vaga';
    protected $fillable = [

        'id_vaga', 'expertise'

    ];

}