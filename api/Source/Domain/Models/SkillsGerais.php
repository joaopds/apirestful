<?php


namespace Source\Domain\Models;


use Illuminate\Database\Eloquent\Model as Eloquent;

class SkillsGerais extends Eloquent
{

    public $timestamps = false;
    protected $table = 'skills';
    protected $fillable = [

        'skill'

    ];

}