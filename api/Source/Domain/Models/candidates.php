<?php

namespace Source\Domain\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Candidate extends Eloquent

{

   public $timestamps = false;
   protected $table = 'candidatos';
   protected $fillable = [

       'first_name', 'last_name', 'jobtitle','age', 'cadastre_date'

   ];

    public function ExpPro()
    {
      return $this->hasMany('exp_profissional_candidatos');
    }
 }