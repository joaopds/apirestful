<?php


namespace Source\Domain\Models;


use Illuminate\Database\Eloquent\Model as Eloquent;

class Recruter extends Eloquent
{

    public $timestamps = false;
    protected $table = 'vaga';
    protected $fillable = [

        'status', 'startDate', 'retributionWished'

    ];
    public function VagaEmprego()
    {
        return $this->hasMany('requiredskills_vaga');
    }

}