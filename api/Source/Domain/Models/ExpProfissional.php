<?php
namespace Source\Domain\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ExpProfissional extends Eloquent
{
    public $timestamps = false;
    protected $table = 'exp_profissional_candidatos';
    protected $fillable = [

        'id_candidato', 'start_date', 'end_date'

    ];
    public function SkillsCandidatos()
    {
        return $this->hasMany('skills_candidatos');
    }
}