<?php
namespace Source\Domain\Models;


use Illuminate\Database\Eloquent\Model as Eloquent;

class Login extends Eloquent

{

   public $timestamps = false;
   protected $table = 'login';
   protected $fillable = [

       'id_candidato', 'user', 'email','password', 'token'

   ];


 }