<?php


namespace Source\Domain\Models;


use Illuminate\Database\Eloquent\Model as Eloquent;

class Skills extends Eloquent
{

    public $timestamps = false;
    protected $table = 'skills_candidatos';
    protected $fillable = [

        'id_expPro', 'id_skill'

    ];
  
}