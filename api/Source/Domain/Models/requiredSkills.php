<?php


namespace Source\Domain\Models;


use Illuminate\Database\Eloquent\Model as Eloquent;

class RequiredSkills extends Eloquent
{

    public $timestamps = false;
    protected $table = 'requiredskills_vaga';
    protected $fillable = [

        'id_vaga', 'id_skill'

    ];

}