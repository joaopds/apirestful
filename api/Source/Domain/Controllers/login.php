<?php
namespace Source\Domain\Controllers;

use Source\Domain\Classes\Validate;
use Source\Domain\Classes\Validations;
use Source\Domain\Models\Login;

class Log
{
   static public function PostLogin()
    {
        $data = json_decode(file_get_contents("php://input"));
        if (!$data) 
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"ERRO"));
            exit;
        }

        $valid = Validate::ValidateLog($data);
        if($valid)
        {
            $login = new Login();
            $login->id_candidato = $data->id_candidato;
            $login->user = $data->user;
            $login->email = $data->email;
            $login->token = GerarToken::token($login, $data);
            $login->senha = $data->password;
            $login->save();
        }
        else
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"ocorreu um erro durante as validações"));
        }

        
    }

    static function loggado() 
    {

        $data = json_decode(file_get_contents("php://input"),false);

        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Nenhum dado inserido!"));
            exit;
        }

        $errors = array();

        if(!Validations::validationString($data->user)) 
        {
            array_push($errors,"Usuário");
        }
        if(!Validations::validationString($data->password))
        {
            array_push($errors,"Senha");
        }
        

        if(count($errors) > 0) 
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"Há campos invalidos no formulario!", "fields"=>$errors));
            exit;
        }

        $VerificaUser = Login::where('user', '=', $data->user)->first('user');
        $VerificaSenha = Login::where('password', '=', $data->password)->first('password');
        $pegarToken = Login::where('user', '=', $data->user)->first('token');
        if(!$VerificaUser || !$VerificaSenha)
        {

            if(!$VerificaUser) 
            {
                echo json_encode(array("resposta"=>"Usuário incorreto, por favor, insira o usuario correto"));
            }
            if(!$VerificaSenha) 
            {
                echo json_encode(array("resposta"=>"senha incorreta, por favor, insira a senha correta"));
            }

            exit;

        }else
        {
            header("HTTP/1.1 201 Created");
            echo json_encode(array("response"=> "Usuário logado com sucesso!"));
            $token = array();
            array_push($token, $pegarToken);
            echo json_encode(array("response"=> $token));

         }

    }

};