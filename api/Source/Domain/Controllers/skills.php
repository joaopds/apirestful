<?php
namespace Source\Domain\Controllers;

use Source\Domain\Classes\Validate;
use Source\Domain\Models\Skill;


class Skills{
  static function AddSkill(){
    $data = json_decode(file_get_contents("php://input"));
    if(!$data){
        header("HTTP/1.1 400 Bad Request");
        echo json_encode(array("response"=>"ERRO"));
        exit;
     }
     $valid = Validate::ValidateSkillsGerais($data);

     if($valid)
     {
         $skills = new Skill();
         $skills->skill = $data->skill;
         $skills->save();
     }


  }
      
  
}