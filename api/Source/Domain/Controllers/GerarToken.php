<?php


namespace Source\Domain\Controllers;


use Firebase\JWT\JWT;

class GerarToken
{
    static function token($login, $data)
    {

        $key = $data->senha;

        $payload = [
            'sub' => $login,

        ];
        $token =JWT::encode($payload, $key) ;
        return $token;
    }

}