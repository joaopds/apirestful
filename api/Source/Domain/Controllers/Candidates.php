<?php
namespace Source\Domain\Controllers;



use Source\Domain\Classes\Validate;
use Source\Domain\Models\Candidate;
use Source\Domain\Models\ExpProfissional;
use Source\Domain\Models\RequiredSkills;
use Source\Domain\Models\Skills;

class Candidates
{

    static function PostC()
    {
        
       $data = json_decode(file_get_contents("php://input"));
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            echo json_encode(array("response"=>"ERRO"));
            exit;
         }

           $valid = Validate::ValidateCandidate($data);
          
            if($valid)
            {
                $candidate = new Candidate();
                $candidate->first_name = $data->first_name;
                $candidate->last_name = $data->last_name;
                $candidate->jobtitle = $data->jobtitle;
                $candidate->age = $data->age;
                $candidate->cadastre_date = $data->cadastre_date;
                $candidate->save();

                $expPro = new ExpProfissional();
                $expPro->id_candidato = $data->id_candidato;
                $expPro->start_date = $data->start_date;
                $expPro->end_date = $data->end_date;
                $expPro->save();

                $skills = new Skills();
                $skills->id_expPro = $data->id_expPro;
                $skills->id_skill = $data->id_skill;
                $skills->save();
            }else{
                header("HTTP1.1/ 400 Bad Request");
                echo json_encode(array("response"=> "erro durante as validações"));
            }     

    }
  
 
 static function GetC()
 {
    $id_skill = filter_input(INPUT_GET, "id_skill");
    $id_skill_vaga = requiredSkills::find($id_skill);
    $users = skills::where('id_skill', '=', $id_skill_vaga)->get();
    if($users)
    {
       // $id = filter_input(INPUT_GET, "id");
        $candidate = Candidate::all();
        $candidatess = array();
        foreach ($candidate as $candidates)
        {
            array_push($candidatess, $candidates) ;
        }
         echo json_encode(array("response"=> $candidatess));
    }
 }


 static function UpdateC()
 {
    $data = json_decode(file_get_contents('php://input'), false);
    if(!$data)
    {
        header("HTTP/1.1 400 Bad Request");
        exit;
    }

    $valid = Validate::ValidateCandidate($data);
   if($valid)
   {
       $id = filter_input(INPUT_GET, 'id');
       $candidate = Candidate::find($id);

       $candidate->first_name = $data->first_name;
       $candidate->last_name = $data->last_name;
       $candidate->jobtitle = $data->jobtitle;
       $candidate->age = $data->age;
       $candidate->cadastre_date = $data->cadastre_date;
       $candidate->save();

       header("HTTP/1.1 201 Created");
   }else{
       header("HTTP 1.1/ 400 Bad Request");
       echo json_encode(array("response"=> "ocorreu um erro durante as validações"));
   }



 }
 static function DeleteSkill()
 {
     $id = filter_input(INPUT_GET, 'id');
     Skills::destroy($id);
 }

 static function DeleteExp()
 {
    $id = filter_input(INPUT_GET, 'id');
    ExpProfissional::destroy($id);
 }
 
 static function DeleteC()
 {
    $id = filter_input(INPUT_GET, 'id');
    Candidate::destroy($id);
 }

}  