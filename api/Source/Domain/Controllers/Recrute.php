<?php

namespace Source\Domain\Controllers;



//use DateTime;
use Source\Domain\Classes\Validate;
use Source\Domain\Models\Expertise;
use Source\Domain\Models\Recruter;
use Source\Domain\Models\RequiredSkills;


class Recrute
{


   static function AddVaga()
   {
    $data = json_decode(file_get_contents("php://input"));
    if(!$data)
    {
        header("HTTP/1.1 400 Bad Request");
        exit;
    }

       $valid = Validate::ValidateVaga($data);

       if($valid)
       {
           $vaga = new Recruter();
           $vaga->status = $data->status;
           $vaga->startDate = $data->startDate;
           $vaga->retributionWished = $data->retributionWished;
           $vaga->save();

           $expertise = new Expertise();
           $expertise->id_vaga = $data->id_vaga;
           $expertise->expertise = $data->expertise;
           $expertise->save();

           $skill = new RequiredSkills();
           $skill->id_vaga = $data->id_vaga;
           $skill->id_skill = $data->id_skill;
           $skill->save();
       }

   }


  static function GetVaga()
  {
    $vagas = Recrute::all();
    if($vagas)
    {
      
        $Vaga = array();
        foreach ($vagas as $vagas) 
        {
            array_push($Vaga, $vagas);
        }
        echo json_encode(array("response"=> $Vaga));
    }
    
    /*function calculaData(){
        $id = filter_input(INPUT_GET, 'id');

        $dates =  ExpProfissional::find($id);

        $experiencia = 0;
        $data1 = new DateTime($dates->start_date);
        $data2 = new DateTime($dates->end_date);
        $experiencia += ($data1->diff($data2))->m;
        echo json_encode(array("response"=>"experiencia de".$experiencia));
    }


        echo calculaData();*/
  }



  static function UpdateVaga(){
  $data = json_decode(file_get_contents('php://input'), false);
  if(!$data)
   {
      header("HTTP/1.1 400 Bad Request");
      exit;
   }

     $valid = Validate::ValidateVaga($data);

     if($valid)
     {
         $id = filter_input(INPUT_GET, 'id');
         $vaga = Recruter::find($id);

         $vaga->status = $data->status;
         $vaga->startDate = $data->startDate;
         $vaga->retributionWished = $data->retributionWished;
         $vaga->save();
     }

   
 }

 static function DeleteRequiredSkills()
 {
     $id = filter_input(INPUT_GET, 'id');
     requiredSkills::destroy($id);
 }

 static function DeleteExpertise()
 {
    $id = filter_input(INPUT_GET, 'id');
 }

 static function DeleteVaga(){
  $id = filter_input(INPUT_GET, 'id');
  Recrute::destroy($id);
 }

}
?>