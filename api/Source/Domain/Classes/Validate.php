<?php
namespace Source\Domain\Classes;



 class Validate
 {
      static function ValidateCandidate($data)
     {
      
           $errors = array();
           
         if(!Validations::validationString($data->first_name))
         {
           array_push($errors, "nome é invalido");
         }
    
         if(!Validations::validationString($data->last_name))
         {
            array_push($errors, "Sobrenome é invalido");
         }
    
         if(!Validations::validationString($data->jobtitle))
         {
             array_push($errors, "titulo é invalido");
         }
    
         if(!Validations::validationInteger($data->age))
         {
             array_push($errors, "idade é invalida");
         }
         if(!Validations::validationString($data->cadastre_date))
         {
             array_push($errors, "data é invalido");
         }
         if(!Validations::validationString($data->start_date))
         {
             array_push ($errors, "skills informadas estao em um formato invalido");
         }
         if(!Validations::validationString($data->end_date))
         {
           array_push($errors, "skills informadas estao em um formato invalido");
         }


         return $data;
     
     }

     static function ValidateVaga()
     {
        $data = json_decode(file_get_contents("php://input"));
        if(!$data)
        {
            header("HTTP/1.1 400 Bad Request");
            exit;
        }
    
         $error = array();
         if(!Validations::ValidationString($data->status))
         {
            array_push($error, "status invalido");
         }
         if(!Validations::validationString($data->startDate))
         {
             array_push($error, "Data de inicio invalida");
         }
         if(!Validations::validationDecimal($data->retributionWished))
         {
             array_push($error, "pretensao salarial invalida");
         }
         if(!Validations::validationString($data->expertise))
         {
         array_push($errors, "expertise invalida");
         }   


         return $data;
     }

     static function ValidateLog($data)
     {
         $data = json_decode(file_get_contents("php://input"));
         if(!$data)
         {
             header("HTTP/1.1 400 Bad Request");
             exit;
         }
         $error = array();
         if(!Validations::validationString($data->user))
         {
             array_push($error, "usuário inválido ou já existente");
         }
         if(!Validations::validationEmail($data->email))
         {
             array_push($error, "email inválido ou já existente");
         }
         if(!Validations::validationString($data->password))
         {
             array_push($error, "senha invalida");
         }
         return $data;
     }

     static function ValidateSkillsGerais($data)
     {
         $error = array();

         if(!Validations::validationString($data->skill))
         {
             array_push($error, "formato de skill invalida");
         }

         return $data;
     }
 }