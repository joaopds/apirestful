<?php

namespace Source\Domain\Classes;

final class Validations
{
    public static function validationString(String $string)
    {
        return strlen($string)>=1 && !is_numeric($string);
    }

    public static function validationInteger(int $integer)
    {
        return filter_var($integer, FILTER_VALIDATE_INT);
    }
    public static function validationDecimal(float $decimal)
    {
        return filter_var($decimal, FILTER_VALIDATE_FLOAT);
    }
    public static function validationEmail(string $email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
?>