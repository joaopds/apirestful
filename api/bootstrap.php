<?php

require "vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;



$capsule = new Capsule;



$capsule->addConnection([

   "driver" => "mysql",

   "host" =>"localhost",

   "database" => "api",

   "username" => "root",

   "port"=> "3308",

   "password" => ""

]);


$capsule->setAsGlobal();

$capsule->bootEloquent();
$capsule->bootEloquent();