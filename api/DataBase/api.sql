-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Tempo de geração: 01-Jun-2020 às 20:28
-- Versão do servidor: 8.0.18
-- versão do PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `api`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `candidatos`
--

DROP TABLE IF EXISTS `candidatos`;
CREATE TABLE IF NOT EXISTS `candidatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `jobtitle` varchar(50) NOT NULL,
  `age` int(2) NOT NULL,
  `cadastre_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `expertise_vaga`
--

DROP TABLE IF EXISTS `expertise_vaga`;
CREATE TABLE IF NOT EXISTS `expertise_vaga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vaga` int(11) NOT NULL,
  `expertise` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_vaga` (`id_vaga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `exp_profissional_candidatos`
--

DROP TABLE IF EXISTS `exp_profissional_candidatos`;
CREATE TABLE IF NOT EXISTS `exp_profissional_candidatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_candidato` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_candidato` (`id_candidato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_candidato` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `token` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `id_candidato` (`id_candidato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `requiredskills_vaga`
--

DROP TABLE IF EXISTS `requiredskills_vaga`;
CREATE TABLE IF NOT EXISTS `requiredskills_vaga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_vaga` int(11) NOT NULL,
  `id_skill` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_vaga` (`id_vaga`),
  KEY `id_skill` (`id_skill`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `skills`
--

DROP TABLE IF EXISTS `skills`;
CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `skills_candidatos`
--

DROP TABLE IF EXISTS `skills_candidatos`;
CREATE TABLE IF NOT EXISTS `skills_candidatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_expPro` int(11) NOT NULL,
  `id_skill` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_expPro` (`id_expPro`),
  KEY `id_skill` (`id_skill`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `vaga`
--

DROP TABLE IF EXISTS `vaga`;
CREATE TABLE IF NOT EXISTS `vaga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `startDate` date NOT NULL,
  `retributionWished` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `expertise_vaga`
--
ALTER TABLE `expertise_vaga`
  ADD CONSTRAINT `expertise_vaga_ibfk_1` FOREIGN KEY (`id_vaga`) REFERENCES `vaga` (`id`);

--
-- Limitadores para a tabela `exp_profissional_candidatos`
--
ALTER TABLE `exp_profissional_candidatos`
  ADD CONSTRAINT `exp_profissional_candidatos_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidatos` (`id`);

--
-- Limitadores para a tabela `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidatos` (`id`);

--
-- Limitadores para a tabela `requiredskills_vaga`
--
ALTER TABLE `requiredskills_vaga`
  ADD CONSTRAINT `requiredskills_vaga_ibfk_1` FOREIGN KEY (`id_vaga`) REFERENCES `vaga` (`id`),
  ADD CONSTRAINT `requiredskills_vaga_ibfk_2` FOREIGN KEY (`id_skill`) REFERENCES `skills` (`id`);

--
-- Limitadores para a tabela `skills_candidatos`
--
ALTER TABLE `skills_candidatos`
  ADD CONSTRAINT `skills_candidatos_ibfk_1` FOREIGN KEY (`id_expPro`) REFERENCES `exp_profissional_candidatos` (`id`),
  ADD CONSTRAINT `skills_candidatos_ibfk_2` FOREIGN KEY (`id_skill`) REFERENCES `skills` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
