<?php

use Phinx\Migration\AbstractMigration;

class CreateExpProfissional extends AbstractMigration
{

    public function up()
    {
      $tab = $this->table("exp_profissional_candidatos");
      $tab->addColumn("id_candidato", "integer", ["limit"=>11]);
      $tab->addColumn("start_date", "date");
      $tab->addColumn("end_date", "date");
      $tab->addForeignKey("id_candidato", "candidatos", "id");
      $tab->save();
    }
    
    public function down()
    {
      $this->dropTable("exp_profissional_candidatos");
    }
}
