<?php

use Phinx\Migration\AbstractMigration;

class CreateExpertise extends AbstractMigration
{
   
    public function up()
    {
      $tab = $this->table("expertise_vaga");
      $tab->addColumn("id_vag", "integer", ["limit"=>11]);
      $tab->addColumn("expertise", "string", ["limit"=>50]);
      $tab->addForeignKey("id_vag", "vaga", "id");
      $tab->save();
    }

    public function down()
    {
       $this->dropTable("expertise_vaga");
    }
}
