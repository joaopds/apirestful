<?php

use Phinx\Migration\AbstractMigration;

class CreateCandidates extends AbstractMigration
{
  
    public function up()
    {
      $tab = $this->table("candidatos");
      $tab->addColumn("first_name", "string", ["limit"=>50]);
      $tab->addColumn("last_name", "string", ["limit"=>50]);
      $tab->addColumn("jobtitle", "string", ["limit"=>50]);
      $tab->addColumn("age", "integer", ["limit"=>11]);
      $tab->addColumn("cadastre_date", "date");
      $tab->save();
    }

    public function Down()
    {
        $this->dropTable("candidatos");
    }
}
