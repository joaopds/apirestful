<?php

use Phinx\Migration\AbstractMigration;

class CreateRequiredSkills extends AbstractMigration
{
    
    public function up()
    {
       $tab = $this->table("required_skills_vaga");
       $tab->addColumn("id_vaga", "integer", ["limit"=>11]);
       $tab->addColumn("id_skill", "integer", ["limit"=>11]);
       $tab->addForeignKey("id_vaga", "vaga", "id");
       $tab->addForeignKey("id_skill", "skills", "id");
       $tab->save();
    }

    public function down()
    {
      $this->dropTable("required_skills_vaga");
    }
}
