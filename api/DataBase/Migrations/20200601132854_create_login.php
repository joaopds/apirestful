<?php

use Phinx\Migration\AbstractMigration;

class CreateLogin extends AbstractMigration
{
   
    public function up()
    {
        $tab = $this->table("login");
        $tab->addColumn("id_candidato", "integer", ["limit"=>11]);
        $tab->addColumn("user", "text", ["limit"=> 10]);
        $tab->addColumn("email", "text", ["limit"=> 20]);
        $tab->addIndex(['user', 'email'], ['unique' => true]);
        $tab->addColumn("senha", "text");
        $tab->addColumn("token", "text");
        $tab->save();
    }

    public function down()
    {
       $this->dropTable("login");
    }
}
