<?php

use Phinx\Migration\AbstractMigration;

class CreateSkillsGerais extends AbstractMigration
{

    public function up()
    {
       $tab = $this->table("skills");
       $tab->addColumn("skill", "string", ["limit"=>50]);
       $tab->save();
    }

    public function down()
    {
       $this->droptable("skills");
    }
}
