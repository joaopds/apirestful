<?php

use Phinx\Migration\AbstractMigration;

class CreateSkillsCandidatos extends AbstractMigration
{
    
    public function up()
    {
      $tab = $this->table("skills_candidatos");
      $tab->addColumn("id_expPro", "integer", ["limit"=>11]);
      $tab->addColumn("id_skill", "integer", ["limit"=>11]);
      $tab->addForeignKey("id_expPro", "exp_profissional_candidatos", "id");
      $tab->addForeignkey("id_skill", "skills", "id");
      $tab->save();
    }
    

    public function down()
    {
      $this->dropTable("skills_candidatos");
    }
}
