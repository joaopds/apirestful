<?php

use Phinx\Migration\AbstractMigration;

class CreateVaga extends AbstractMigration
{
   
    public function up()
    {
       $tab = $this->table("vaga");
       $tab->addColumn("status", "boolean");
       $tab->addColumn("startDate", "date");
       $tab->addColumn("retributionWished", "decimal");
       $tab->save();
    }

    public function down()
    {
      $this-dropTable("vaga");
    }
}
