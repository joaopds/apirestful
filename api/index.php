<?php


use Illuminate\Support\Facades\Auth;
use infrastructure\Middleware\AuthorizationMiddleware;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Source\Domain\Controllers\Candidates;
use Source\Domain\Controllers\Skills;
use Source\Domain\Controllers\Recrute;
use Source\Domain\Controllers\Log;


require __DIR__ . "/vendor/autoload.php";
require __DIR__ . "/bootstrap.php";
require __DIR__ . "/Source/Domain/Models/Candidates.php";
require __DIR__ . "/Source/Domain/Controllers/Recrute.php";
require __DIR__ . "/Source/Domain/Controllers/Candidates.php";
require __DIR__ . "/Source/Domain/Controllers/skills.php";
require __DIR__ . "/Source/Domain/Classes/Validations.php";
require __DIR__ . "/Source/Domain/Models/expertise.php";
require __DIR__ . "/Source/Domain/Models/requiredSkills.php";
require __DIR__ . "/Source/Domain/Models/SkillsGerais.php";
require __DIR__ . "/Source/Domain/Models/ExpProfissional.php";
require __DIR__ . "/Source/Domain/Models/Skills.php";
require __DIR__ . "/Source/infrastructure/Middleware/AuthorizationMiddleware.php";
require __DIR__ . "/Source/Domain/Controllers/GerarToken.php";
require __DIR__ . "/Source/Domain/Controllers/login.php";
require __DIR__ . "/Source/Domain/Classes/Validate.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$configuration = [
    'settings' =>[
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
                      
      $app->get('/candidato', function (Request $request, Response $response) {

        Candidates::GetC();
      });

      $app->post('/candidato', function (Request $request, Response $response) {

        Candidates::PostC();
      });

      $app->put('/candidato', function (Request $request, Response $response) {

        Candidates::UpdateC();
      });

      $app->delete('/candidato', function (Request $request, Response $response) {

        Candidates::DeleteC();
      });

      $app->delete('/candidato/exp', function (Request $request, Response $response) {

        Candidates::DeleteExp();
      });

      $app->delete('/candidato/skills', function (Request $request, Response $response) {

        Candidates::DeleteSkill();
      });

      $app->post('/login', function (Request $request, Response $response){
        
        Log::PostLogin();
      });
      $app->get('/logar', function (Request $request, Response $response){
         
        Log::loggado();
      });

      $app->group('', function() use ($app){
       
      $app->get('/Vaga', function (Request $request, Response $response) {
      
          Recrute::GetVaga();
      });
  
      $app->post('/Vaga', function (Request $request, Response $response) {

          Recrute::AddVaga();
      });
  
      $app->put('/Vaga', function (Request $request, Response $response) {

          Recrute::UpdateVaga();
      });
  
      $app->delete('/Vaga', function (Request $request, Response $response) {

          Recrute::DeleteVaga();
      });

      $app->delete('/Vaga/expertise', function (Request $request, Response $response) {

          Recrute::DeleteExpertise();
      });

      $app->delete('/Vaga/requiredskills', function (Request $request, Response $response) {
        
          Recrute::DeleteRequiredSkills();
      });

      })->add(AuthorizationMiddleware::authorizationToken());
         
      $app->post('/Skill', function (Request $request, Response $response) {

         Skills::AddSkill();
      
      });
    $app->run();